package resolution.example6.zzeulki.practice122;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;


/**
 * A simple {@link Fragment} subclass.
 */
public class map extends Fragment implements OnMapReadyCallback{

    private MapView mapView;
    private GoogleMap googleMap;

    public map() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_map,container,false);
        mapView = (MapView) rootView.findViewById(R.id.map);
        mapView.onCreate(savedInstanceState);
        mapView.onResume();
        mapView.getMapAsync(this);

        return rootView;
    }

    @Override
    public void onMapReady(GoogleMap googleMap){
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(35.886869, 128.608408), 16));
        googleMap.addMarker(new MarkerOptions().position(new LatLng(35.886869,128.608408)).title("school"));


        MarkerOptions marker[] = {
                new MarkerOptions().position(new LatLng(35.888345, 128.609668)).title("shop").snippet("shop!"),
                new MarkerOptions().position(new LatLng(35.886971, 128.609196)).title("shop2").snippet("shop2!"),
                new MarkerOptions().position(new LatLng(35.891792, 128.610631)).title("shop3").snippet("shop3!"),
        };


        for (int i = 0; i < 3; ++i) {
            marker[i].icon(BitmapDescriptorFactory.fromResource(R.drawable.lace2));
            googleMap.addMarker(marker[i]);
        }

    }



}
